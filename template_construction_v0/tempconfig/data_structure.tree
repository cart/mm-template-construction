ext_nii=.nii.gz
ext_mat=.mat
ext_ini=.ini
ext_txt=.txt

!{data_dir}
    {sub_id} (data_sub_dir)
        T1 (sub_T1_dir)
            T1_unbiased_brain{ext_nii} (T1_brain)
            T1_brain_mask{ext_nii} (T1_brain_mask)
            T1_unbiased{ext_nii} (T1_head)
        T2_FLAIR (sub_T2_dir)
            T2_FLAIR_unbiased{ext_nii} (T2_head)
        dMRI (sub_dMRI1_dir)
            dMRI (sub_dMRI2_dir)
                dti_tensor{ext_nii} (DTI_tensor)
!{config_dir}
    mmorf_params_step01{ext_ini} (mmorf_params_01)
    mmorf_params_step02{ext_ini} (mmorf_params_02)
    mmorf_params_step03{ext_ini} (mmorf_params_03)
    mmorf_params_step04{ext_ini} (mmorf_params_04)
    mmorf_params_step05{ext_ini} (mmorf_params_05)
    mmorf_params_step06{ext_ini} (mmorf_params_06)
!{preprocessed_dir}
    {sub_id}_T2_to_T1{ext_mat} (T2_to_T1_mat)
    {sub_id}_DTI_to_T1{ext_mat} (DTI_to_T1_mat)
!{template_dir}
    misc (misc_dir)
        ident{ext_mat} (identity_mat)
    log (log_dir)
    scripts (script_dir)
    clamped_images (clamped_dir)
        {sub_id} (clamped_sub_dir)
            {sub_id}_T1_clamped{ext_nii} (T1_head_clamped)
    affine_step1 (affine_it1_dir)
        {sub_id} (affine_it1_subject_dir)
            T1_to_{ref_id}{ext_mat} (T1_to_ref_mat)
            T1_to_{ref_id}{ext_nii} (T1_to_ref_img)
            T1_to_unbiased{ext_mat} (T1_to_unbiased_mat)
            T1_to_unbiased{ext_nii} (T1_to_unbiased_img)
            T2_to_unbiased{ext_mat} (T2_to_unbiased_mat)
            DTI_to_unbiased{ext_mat} (DTI_to_unbiased_mat)
        T1_unbiased_affine_template{ext_nii} (T1_unbiased_affine_template)
        T1_unbiased_affine_template_to_MNI{ext_nii} (T1_unbiased_affine_template_to_MNI_img)
        T1_unbiased_affine_template_to_MNI{ext_mat} (T1_unbiased_affine_template_to_MNI_mat)
        unbiasing_matrix_inverse{ext_mat} (T1_unbiasing_affine_matrix)
    affine_step2 (affine_it2_dir)
        {sub_id} (affine_it2_subject_dir)
            T1_to_MNI152{ext_mat} (T1_to_MNI_mat)
            T1_brain_to_MNI152{ext_nii} (T1_brain_to_MNI_img)
            T1_head_to_MNI152{ext_nii} (T1_head_to_MNI_img)
            T1_brain_mask_to_MNI152{ext_nii} (T1_brain_mask_to_MNI_img)
            T2_FLAIR_to_MNI152{ext_mat} (T2_to_MNI_mat)
            T2_FLAIR_brain_to_MNI152{ext_nii} (T2_brain_to_MNI_img)
            T2_FLAIR_head_to_MNI152{ext_nii} (T2_head_to_MNI_img)
            DTI_to_MNI152{ext_mat} (DTI_to_MNI_mat)
            DTI_to_MNI152{ext_nii} (DTI_to_MNI_img)
            DTI_tensor_to_MNI152{ext_nii} (DTI_tensor_to_MNI)
        affine_template_T1_brain{ext_nii} (T1_brain_affine_template)
        affine_template_T1_brain_mask{ext_nii} (T1_brain_mask_affine_template)
        affine_template_T1_brain_mask_weighted{ext_nii} (T1_brain_mask_weighted_affine_template)
        affine_template_DTI_brain_mask_weighted{ext_nii} (DTI_brain_mask_weighted_affine_template)
        affine_template_T1_head{ext_nii} (T1_head_affine_template)
        affine_template_T2_head{ext_nii} (T2_head_affine_template)
        affine_template_DTI{ext_nii} (DTI_affine_template)
        affine_template_DTI_tensor{ext_nii} (DTI_tensor_affine_template)
    nln_step_{step_id} (nln_step_dir)
        iteration_{it_id} (nln_step_iteration_dir)
            {sub_id}_to_template_warp{ext_nii} (mmorf_warp)
            {sub_id}_to_template_warp_resampled{ext_nii} (mmorf_warp_resampled)
            {sub_id}_to_template_warp_resampled_unbiased{ext_nii} (mmorf_warp_resampled_unbiased)
            {sub_id}_to_template_warp_resampled_unbiased_full_T1_brain{ext_nii} (mmorf_warp_resampled_unbiased_full_T1brain)
            {sub_id}_to_template_warp_resampled_unbiased_full_T1_head{ext_nii} (mmorf_warp_resampled_unbiased_full_T1head)
            {sub_id}_to_template_warp_resampled_unbiased_full_T2{ext_nii} (mmorf_warp_resampled_unbiased_full_T2)
            {sub_id}_to_template_warp_resampled_unbiased_full_DTI{ext_nii} (mmorf_warp_resampled_unbiased_full_DTI)
            {sub_id}_to_template_jac{ext_nii} (mmorf_jac)
            {sub_id}_to_template_bias (mmorf_bias)
            average_warp{ext_nii} (avg_warp)
            average_invwarp{ext_nii} (inv_avg_warp)
            {sub_id}_to_template_T1_brain{ext_nii} (warped_T1brain)
            {sub_id}_to_template_T1_head{ext_nii} (warped_T1head)
            {sub_id}_to_template_T2_head{ext_nii} (warped_T2head)
            {sub_id}_to_template_DTI_scalar{ext_nii} (warped_DTIscalar)
            {sub_id}_to_template_DTI_tensor{ext_nii} (warped_DTItensor)
            {sub_id}_to_template_T1_brain_mask{ext_nii} (warped_T1brain_mask)
            nln_template_T1_brain_{step_id}_{it_id}{ext_nii} (T1_brain_nln_template)
            nln_template_T1_brain_mask_{step_id}_{it_id}{ext_nii} (T1_brain_mask_nln_template)
            nln_template_T1_brain_mask_weighted_{step_id}_{it_id}{ext_nii} (T1_brain_mask_weighted_nln_template)
            nln_template_DTI_brain_mask_weighted{ext_nii} (DTI_brain_mask_weighted_nln_template)
            nln_template_T1_brain_SD_{step_id}_{it_id}{ext_nii} (T1_brain_SD_nln_template)
            nln_template_T1_head_SD_{step_id}_{it_id}{ext_nii} (T1_head_SD_nln_template)
            nln_template_T1_head_{step_id}_{it_id}{ext_nii} (T1_head_nln_template)
            nln_template_T2_head_{step_id}_{it_id}{ext_nii} (T2_head_nln_template)
            nln_template_DTI_{step_id}_{it_id}{ext_nii} (DTI_nln_template)
            nln_template_DTI_tensor_{step_id}_{it_id}{ext_nii} (DTI_tensor_nln_template)
            nln_RMS_delta_intensity_{step_id}_{it_id}{ext_txt} (delta_intensity_output)
            nln_RMS_delta_avgwarp_{step_id}_{it_id}{ext_txt} (delta_avgwarp_output)
            nln_RMS_intensity_sd_{step_id}_{it_id}{ext_txt} (sd_intensity_output)
