#!/usr/bin/env python
#
# run_template_construction.py - Construction of multimodal templates
#
# Author: Christoph Arthofer <c.arthofer@gmail.com, christoph.arthofer@ndcn.ox.ac.uk>
# Copyright: FMRIB 2021
#
"""! This script allows the construction of an unbiased, multimodal template from T1, T1+T2 or T1+T2+DTI modalities.
"""

import os
import shutil
import pandas as pd
import nibabel as nib
import numpy as np
import sys
from fsl.wrappers import fslmaths,flirt,applyxfm,concatxfm,bet,fast,fslstats
from fsl.data.image import Image
from fsl.wrappers.fnirt import invwarp, applywarp, convertwarp
from file_tree import FileTree
from fsl.utils.run import func_to_cmd
from operator import itemgetter
import argparse
import fsl_sub

def correctBiasMidtransWrapper(aff_matrix_paths, temp_dir, ref_path, unbiasing_invmtx_path, unbiased_matrix_paths):
    """! Finds the midspace of all affine transformations to avoid bias towards a reference image in the affine template construction process.

    @param aff_matrix_paths:              List of filepaths to affine transformations
    @param temp_dir:                      Output directory
    @param ref_path:                      Path to reference template
    @param unbiasing_invmtx_path:         Path to unbiasing matrix
    @param unbiased_matrix_paths:         List of filepaths to unbiased transformations

    """
    separate_path = os.path.join(temp_dir, 'T1_to_unbiased')
    command = 'midtrans -v --separate=' + separate_path + ' --template=' + ref_path + ' -o ' + unbiasing_invmtx_path + ' '
    count = 0
    for omat_path in aff_matrix_paths:
        if os.path.exists(omat_path):
            count += 1
            print(count, ' ', omat_path)
            command += omat_path + ' '

    stream = os.popen(command)
    output = stream.read()
    print(output)

    # Renaming matrices
    for i, sub_unbiasing_mat in enumerate(unbiased_matrix_paths):
        sub_unbiasing_mat_temp = os.path.join(temp_dir, 'T1_to_unbiased%04d.mat' % (i + 1))

        os.rename(sub_unbiasing_mat_temp, sub_unbiasing_mat)
        print(i, ' ', sub_unbiasing_mat_temp, ' renamed to ', sub_unbiasing_mat)

    print('T1 unbiasing matrices constructed!')

# ------------------------------------------------tensor averaging---------------------------------------------------------------------------
# Date: 21/09/2022
# Author: Frederik J Lange
# Copyright: FMRIB 2022

# Performs a fast matrix logarithm using Eigen decomposition. NB! Assumes matrix is SPD and does not check if matrix is zero.
def fast_logm(A):
    A_mask = ~np.isfinite(A)
    A[A_mask] = 0
    W, V = np.linalg.eigh(A)
    log_A = np.matmul(V * np.log(W)[..., None, :], np.linalg.inv(V))
    log_A[~np.isfinite(log_A)] = np.nan
    log_A[A_mask] = np.nan
    return log_A

# Performs a fast matrix exponention using Eigen decomposition. NB! Assumes matrix is symmetric.
def fast_expm(A):
    A_mask = ~np.isfinite(A)
    A[A_mask] = 0
    W, V = np.linalg.eigh(A)
    exp_A = np.matmul(V * np.exp(W)[..., None, :], np.linalg.inv(V))
    exp_A[~np.isfinite(exp_A)] = np.nan
    exp_A[A_mask] = np.nan
    return exp_A

# Convert linearised upper-diagonal 3x3 matrix to full 3x3 matrix
def tensor_fsl_to_full(tensor_fsl):
    tensor_full = np.zeros(tensor_fsl.shape[0:3] + (3, 3))
    tensor_full[..., 0, 0] = tensor_fsl[..., 0]
    tensor_full[..., 0, 1] = tensor_full[..., 1, 0] = tensor_fsl[..., 1]
    tensor_full[..., 0, 2] = tensor_full[..., 2, 0] = tensor_fsl[..., 2]
    tensor_full[..., 1, 1] = tensor_fsl[..., 3]
    tensor_full[..., 1, 2] = tensor_full[..., 2, 1] = tensor_fsl[..., 4]
    tensor_full[..., 2, 2] = tensor_full[..., 2, 2] = tensor_fsl[..., 5]
    return tensor_full

# Convert full 3x3 matrix to linearised upper-diagonal matrix
def tensor_full_to_fsl(tensor_full):
    tensor_fsl = np.zeros(tensor_full.shape[0:3] + (6,))
    tensor_fsl[..., 0] = tensor_full[..., 0, 0]
    tensor_fsl[..., 1] = tensor_full[..., 0, 1]
    tensor_fsl[..., 2] = tensor_full[..., 0, 2]
    tensor_fsl[..., 3] = tensor_full[..., 1, 1]
    tensor_fsl[..., 4] = tensor_full[..., 1, 2]
    tensor_fsl[..., 5] = tensor_full[..., 2, 2]
    return tensor_fsl

# Calculate the average log-tensor. NB! All tensors must have the same 3D dimenstions.
def log_tensor_average(tensor_paths, output_path):
    avg_tensor_img = Image(tensor_paths[0])
    avg_tensor_data = np.zeros(avg_tensor_img.shape[0:3] + (3, 3))
    valid_tensor_data = np.zeros(avg_tensor_img.shape[0:3] + (3, 3))

    for i, tensor_path in enumerate(tensor_paths, start=1):
        print('Processing tensor', i, 'of', len(tensor_paths))
        tensor_img = Image(tensor_path)
        tensor_data = tensor_fsl_to_full(1e6 * tensor_img[:])
        log_tensor_data = fast_logm(tensor_data)
        log_tensor_nans = np.isnan(log_tensor_data)
        avg_tensor_data[~log_tensor_nans] = avg_tensor_data[~log_tensor_nans] + log_tensor_data[~log_tensor_nans]
        valid_tensor_data = valid_tensor_data + (~log_tensor_nans).astype(int)

    avg_tensor_data[valid_tensor_data > 0] = avg_tensor_data[valid_tensor_data > 0] / valid_tensor_data[valid_tensor_data > 0]
    avg_tensor_data = fast_expm(avg_tensor_data)
    avg_tensor_data[valid_tensor_data <= 0] = 0
    avg_tensor_data = tensor_full_to_fsl(1e-6 * avg_tensor_data)
    avg_tensor_img[:] = avg_tensor_data

    avg_tensor_img.save(output_path)

# Calculate the weighted average log-tensor. NB! All tensors and weights must have the same 3D dimenstions.
def log_tensor_average_weighted(tensor_paths, weight_paths, output_path):
    avg_tensor_img = Image(tensor_paths[0])
    avg_tensor_data = np.zeros(avg_tensor_img.shape[0:3] + (3, 3))
    valid_tensor_data = np.zeros(avg_tensor_img.shape[0:3] + (3, 3))

    for i, (tensor_path, weight_path) in enumerate(zip(tensor_paths, weight_paths), start=1):
        print('Processing tensor', i, 'of', len(tensor_paths))
        tensor_img = Image(tensor_path)
        tensor_data = tensor_fsl_to_full(1e6 * tensor_img[:])
        log_tensor_data = fast_logm(tensor_data)
        log_tensor_nans = np.isnan(log_tensor_data)
        weight_img = Image(weight_path)
        weight_img_data = weight_img[:]
        weight_img_data = np.repeat(weight_img_data[:, :, :, np.newaxis], 3, axis=3)
        weight_img_data = np.repeat(weight_img_data[:, :, :, :, np.newaxis], 3, axis=4)
        avg_tensor_data[~log_tensor_nans] = avg_tensor_data[~log_tensor_nans] + log_tensor_data[~log_tensor_nans] * weight_img_data[~log_tensor_nans]
        valid_tensor_data = valid_tensor_data + (~log_tensor_nans).astype(int) * weight_img_data

    avg_tensor_data[valid_tensor_data > 0] = avg_tensor_data[valid_tensor_data > 0] / valid_tensor_data[valid_tensor_data > 0]
    avg_tensor_data = fast_expm(avg_tensor_data)
    avg_tensor_data[valid_tensor_data <= 0] = 0
    avg_tensor_data = tensor_full_to_fsl(1e-6 * avg_tensor_data)
    avg_tensor_img[:] = avg_tensor_data

    avg_tensor_img.save(output_path)

# ---------------------------------------------------------------------------------------------------------------------------

def averageImages(img_paths, out_path, biasfield_paths=None, mod='mean', norm_bool=False):
    """! Creates an average image from individual (non)normalised images.

    @param img_paths:             List of filepaths
    @param out_path:              Path to average/median output image
    @param biasfield_paths:       List of filepaths to corresponding bias fields
    @param mode:                  Choose between 'mean' or 'median' image.
    @param norm_bool:             Normalise intensities of each image before averaging true or false

    """

    n_imgs = len(img_paths)
    
    if biasfield_paths is not None:
        n_biasfields = len(biasfield_paths)
        assert n_biasfields == n_imgs, "The number of bias fields does not match the number of images."

    n_exist = 0
    if mod == 'mean':
        for i, img_path in enumerate(img_paths):
            if os.path.exists(img_path):
                n_exist += 1
                print(i, ' ', img_path)
                img_nib = nib.load(img_path)

                if biasfield_paths is not None:
                    biasfield_nib = nib.load(biasfield_paths[i])
                    img_nib = fslmaths(img_nib).div(biasfield_nib).run()

                if norm_bool:
                    img_nib = fslmaths(img_nib).inm(1000).run()
                if i == 0:
                    sum_img = img_nib
                else:
                    sum_img = fslmaths(sum_img).add(img_nib).run()
            else:
                print(i, ' ', img_path, ' does not exist!')

        if n_exist > 0:
            mean_img = fslmaths(sum_img).div(n_exist).run()
            mean_img.to_filename(out_path)
    elif mod == 'median':
        images = []
        for i, img_path in enumerate(img_paths):
            if os.path.exists(img_path):
                n_exist += 1
                print(i, ' ', img_path)
                img_nib = nib.load(img_path)

                if biasfield_paths is not None:
                    biasfield_nib = nib.load(biasfield_paths[i])
                    img_nib = fslmaths(img_nib).div(biasfield_nib).run()
                
                if norm_bool:
                    img_nib = fslmaths(img_nib).inm(1000).run()
                images.append(img_nib.get_fdata())
        if n_exist > 0:
            median_img = np.median(np.array(images),axis=0)
            median_nib = nib.Nifti1Image(np.squeeze(median_img), affine=img_nib.affine, header=img_nib.header)
            median_nib.to_filename(out_path)

    assert n_exist == n_imgs, "Not all images available!"


def applyWarpWrapper(img_path, ref_path, warped_path, warp_path, interp='spline', norm_bool=False, relative_warp=None):
    """! Wrapper for FSL applywarp - applies a warp (deformation field) to an image.

    @param img_path:              Path to input image
    @param ref_path:              Path to reference image
    @param warped_path:           Path to warped output image
    @param warp_path:             Path to warp (deformation field)
    @param interp:                Interpolation method (same options as FSL applywarp)
    @param norm_bool:             Normalise intensities of each image before warping true or false
    @param relative_warp:         Uses relative warp convention if True; otherwise it uses the default option

    """

    print('Applying ', warp_path, ' to ', img_path)
    if os.path.exists(img_path):
        img_nib = nib.load(img_path)
        if norm_bool:
            img_nib = fslmaths(img_nib).inm(1000).run()
        if relative_warp is not None:
            applywarp(src=img_nib, ref=ref_path, out=warped_path, warp=warp_path, interp=interp, rel=relative_warp)
        else:
            applywarp(src=img_nib, ref=ref_path, out=warped_path, warp=warp_path, interp=interp)


def submitJob(command, name, log_dir, queue, wait_for=None, array_task=False, coprocessor=None, coprocessor_class=None, coprocessor_multi="1", threads=1, export_var=None):
    """! Wrapper for fslsub (https://git.fmrib.ox.ac.uk/fsl/fsl_sub) - submits a job to the cluster. This function can be easily extended to work with other workload managers.

    @param name:                  Job name
    @param log_dir:               Directory where output log-files will be saved
    @param queue:                 Name of queue to submit the job to
    @param wait_for:              List of IDs of jobs required to finish before running this job.
    @param script:                Path to a shell script, which contains one command per line - commands will be submitted as an array job
    @param command:               Alternatively a single command can be provided as a string - command will be submitted as single job
    @param coprocessor_class:     Coprocessor class
    @param export_var:            Environment variables to be exported to the submission node
    @param debug:                 If True, information about job will be written to output

    @return  The job ID.
    """
    coprocessor_class_strict = True if coprocessor_class is not None else False

    hold_ids = []
    if wait_for is not None:
        for job_id in wait_for:
            if len(job_id) > 0:
                hold_ids.append(job_id)

    job_id = fsl_sub.submit(command=command,
                   array_task=array_task,
                   jobhold=hold_ids,
                   name=name,
                   logdir=log_dir,
                   queue=queue,
                   coprocessor=coprocessor,
                   coprocessor_class=coprocessor_class,
                   coprocessor_class_strict=coprocessor_class_strict,
                   coprocessor_multi=coprocessor_multi,
                   threads=threads,
                   export_vars=export_var
                   )

    return str(job_id)


def RMSdifference(img1_path, img2_path, mask1_path=None, mask2_path=None, rms_path=None):
    """! Calculates the difference between two images or warps as the root mean squared (RMS)

    @param img1_path:                 Path to first image or deformation field
    @param img2_path:                 Path to second image or deformation field
    @param mask1_path:                Path to mask for first image
    @param mask2_path:                Path to mask for second image
    @param rms_path:                  Path to output text file that RMS is written to

    """

    img1_arr = nib.load(img1_path).get_fdata()
    img2_arr = nib.load(img2_path).get_fdata()

    if mask1_path is not None and mask2_path is not None:
        mask1_arr = nib.load(mask1_path).get_fdata()
        mask2_arr = nib.load(mask2_path).get_fdata()

        if len(img1_arr.shape) > 3:
            n_dim = img1_arr.shape[-1]
            img1_mask_stack_arr = np.stack((mask1_arr,) * n_dim, -1)
            n_dim = img2_arr.shape[-1]
            img2_mask_stack_arr = np.stack((mask2_arr,) * n_dim, -1)
        else:
            img1_mask_stack_arr = mask1_arr
            img2_mask_stack_arr = mask2_arr

        img_mask_stack_arr = np.logical_or(img1_mask_stack_arr, img2_mask_stack_arr)
        img1_masked_arr = img1_arr[img_mask_stack_arr > 0]
        img2_masked_arr = img2_arr[img_mask_stack_arr > 0]

        diff_img = img1_masked_arr - img2_masked_arr
    else:
        diff_img = img1_arr - img2_arr

    dim = np.prod(diff_img.shape)
    rms = np.sqrt((diff_img ** 2).sum() / dim)

    print('RMS difference between {} and {}: {}'.format(img1_path, img2_path, rms))
    if rms_path is not None:
        with open(rms_path, 'w+') as f:
            f.write('{}'.format(rms))


def RMSstandardDeviation(img_paths, mean_img_path, mask_path, sd_img_out_path=None, rms_out_path=None):
    """! Calculates the standard deviation of images as the root mean squared (RMS) (== coefficient of variation)

    @param img_paths:                     List of paths to images
    @param mean_img_path:                 Path to average image
    @param mask_path:                     Path to mask
    @param sd_img_out_path:               Path to standard deviation output image
    @param rms_out_path:                  Path to output text file that RMS is written to

    """

    mean_img_nib = nib.load(mean_img_path)

    for i, path in enumerate(img_paths):
        print(i, ' ', path)
        diff_img = fslmaths(path).inm(1000).sub(mean_img_nib).run()
        if i == 0:
            diffsum_img = fslmaths(diff_img).mul(diff_img).run()
        else:
            diffsum_img = fslmaths(diff_img).mul(diff_img).add(diffsum_img).run()

    stdtemp_img = fslmaths(diffsum_img).div(len(img_paths)).run()
    stdtemp_img_np = np.sqrt(stdtemp_img.get_fdata())
    if sd_img_out_path is not None:
        std_img = nib.Nifti1Image(stdtemp_img_np, affine=stdtemp_img.affine, header=stdtemp_img.header)
        std_img.to_filename(sd_img_out_path)

    mask_np = nib.load(mask_path).get_fdata()
    mean_img_np = mean_img_nib.get_fdata()

    stdtemp_img_masked_np = stdtemp_img_np[mask_np > 0]
    mean_img_masked_np = mean_img_np[mask_np > 0]

    cv = stdtemp_img_masked_np / mean_img_masked_np  # coefficient of variation
    dim = np.prod(cv.shape)
    rms = np.sqrt((cv ** 2).sum() / dim)

    print('RMS standard deviation: {}'.format(rms))
    if rms_out_path is not None:
        with open(rms_out_path, 'w+') as f:
            f.write('{}'.format(rms))


def mmorfWrapper(mmorf_run_cmd, config_path, img_warp_space,
                 img_ref_scalar, img_mov_scalar, aff_ref_scalar, aff_mov_scalar,
                 mask_ref_scalar, mask_mov_scalar,
                 img_ref_tensor, img_mov_tensor, aff_ref_tensor, aff_mov_tensor,
                 mask_ref_tensor, mask_mov_tensor,
                 warp_out, jac_det_out, bias_out):
    """! Wrapper function for running MMORF.

    @param mmorf_run_cmd:                     Command to run MMORF
    @param config_path:                       Path to config file with fixed parameters
    @param img_warp_space:                    Path to image defining the space in which the warp field will be calculated
    @param img_ref_scalar:                    List of paths to scalar reference images
    @param img_mov_scalar:                    List of paths to scalar moving images
    @param aff_ref_scalar:                    List of paths to affine transformations for scalar reference images
    @param aff_mov_scalar:                    List of paths to affine transformations for scalar moving images
    @param mask_ref_scalar:                   List of paths to masks in reference image spaces
    @param mask_mov_scalar:                   List of paths to masks in moving image spaces
    @param img_ref_tensor:                    List of paths to reference tensors
    @param img_mov_tensor:                    List of paths to moving tensors
    @param aff_ref_tensor:                    List of paths to affine transformations for reference tensors
    @param aff_mov_tensor:                    List of paths to affine transformations for moving tensors
    @param mask_ref_tensor:                   List of paths to masks in reference tensor spaces
    @param mask_mov_tensor:                   List of paths to masks in moving tensor spaces
    @param warp_out:                          Path to output warp field
    @param jac_det_out:                       Path to output Jacobian determinant of final warp field
    @param bias_out:                          Path to output bias field for scalar image pairs

    @return  The command as a string and a dictionary of environment variables.

    """

    export_var = []
    cmd = mmorf_run_cmd
    cmd += ' --config ' + config_path
    split = os.path.split(config_path)
    export_var.append(split[0])
    cmd += ' --img_warp_space ' + img_warp_space
    split = os.path.split(img_warp_space)
    export_var.append(split[0])
    for path in img_ref_scalar:
        cmd += ' --img_ref_scalar ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in img_mov_scalar:
        cmd += ' --img_mov_scalar ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in aff_ref_scalar:
        cmd += ' --aff_ref_scalar ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in aff_mov_scalar:
        cmd += ' --aff_mov_scalar ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in mask_ref_scalar:
        cmd += ' --mask_ref_scalar ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in mask_mov_scalar:
        cmd += ' --mask_mov_scalar ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in img_ref_tensor:
        cmd += ' --img_ref_tensor ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in img_mov_tensor:
        cmd += ' --img_mov_tensor ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in aff_ref_tensor:
        cmd += ' --aff_ref_tensor ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in aff_mov_tensor:
        cmd += ' --aff_mov_tensor ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in mask_ref_tensor:
        cmd += ' --mask_ref_tensor ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    for path in mask_mov_tensor:
        cmd += ' --mask_mov_tensor ' + path
        split = os.path.split(path)
        export_var.append(split[0])
    cmd += ' --warp_out ' + warp_out
    split = os.path.split(warp_out)
    export_var.append(split[0])
    cmd += ' --jac_det_out ' + jac_det_out
    split = os.path.split(jac_det_out)
    export_var.append(split[0])
    cmd += ' --bias_out ' + bias_out
    split = os.path.split(bias_out)
    export_var.append(split[0])

    cmd += '\n'

    export_var = list(filter(None, list(set(export_var))))
    export_var = {'SINGULARITY_BIND': export_var}

    return cmd, export_var

if __name__ == "__main__":
    """! Main function submitting the jobs.
    """
    identity_path = os.getenv('FSLDIR')+'/etc/flirtsch/ident.mat'

    flags_required = {
        'input_scalar': [('-input_scalar','--input_scalar'),'<dir>'],
        'config': [('-config','--config'),'<dir>'],
        'tree': [('-tree','--tree'), '<path>'],
        'subids': [('-subids','--subids'),'<path>'],
        'output': [('-o', '--output'),'<dir>'],
        'affine': [('-aff', '--affine'), '[True,False]'],
        'dof': [('-dof', '--dof'), '[6,7,9,12]'],
        'nonlinear': [('-nln', '--nonlinear'), '[True,False]'],
        'n_resolutions': [('-nres', '--n_resolutions'), '<int>'],
        'n_iterations': [('-nit', '--n_iterations'), '<int>'],
        'standardspace': [('-stdspace', '--standardspace'),'<path>'],
        'cpuq': [('-cpuq','--cpuq'), '<string>']
    }
    help_required = {
        'input_scalar': 'Paths to the directories containing the scalar modalities. The first modality will be used as the primary modality i.e. to create the affine template and to register rigidly to a standard space template if provided',
        'config': 'Directory containing the MMORF parameter files',
        'tree': 'Path to FSL Filetree describing the subject-specific directory structure',
        'subids': 'Path to .csv file containing one subject ID per row: subject IDs have to indentify the sub-directories of the \'input\' argument (optional) \n if not provided all sub-directories of the \'input\' argument will be used',
        'output': 'Output directory',
        'affine': 'Run affine template construction (required for affine)',
        'dof': 'Number of degrees of freedom used to register to the standard space.',
        'nonlinear': 'Run nonlinear template construction (required for nonlinear)',
        'n_resolutions': 'Number of resolution levels (has to be <= number of resolutions defined in the MMORF config (required for nonlinear template construction)',
        'n_iterations': 'Number of iterations per resolution level (required for nonlinear template construction)',
        'standardspace': 'The template will be aligned to this space by rigidly registering the volume of the primary modality and, hence, this space should be the same modality as the primary modality. This could be a path to, for example, the brain-extracted T1 volume of the OMM-1 template if the primary modality in the tree is T1, or the brain-extracted T1 MNI template.',
        'cpuq': 'Name of cluster queue to submit CPU jobs to (required for affine and nonlinear template construction)'
    }

    flags_optional = {
        'input_tensor': [('-input_tensor','--input_tensor'),'<dir> <dir> ...'],
        'mmorf': [('-m', '--mmorf'), '<path>'],
        'gpuq': [('-gpuq','--gpuq'), '<string>'],
        'fslsub_conf': [('-fslsub_conf','--fslsub_conf'), '<string>']
    }
    help_optional = {
        'input_tensor': 'Paths to the directories containing the tensor modalities.',
        'mmorf': 'Path to MMORF singularity container. Required for nonlinear template if MMORF is not installed via conda.',
        'gpuq': 'Name of cluster queue to submit GPU jobs to (required for nonlinear template construction)',
        'fslsub_conf': 'Path that will be assigned to the FSLSUB_CONF environment variable.'
    }

    parser = argparse.ArgumentParser(prog='run_template_construction.py',
                                     description='Constructs a multimodal template from scalar and tensor-valued modalities.',
                                     usage='\npython run_template_construction.py --input_scalar /path/to/imaging/data/subjectsAll/T1/ /path/to/imaging/data/subjectsAll/FLAIR/ --input_tensor /path/to/imaging/data/subjectsAll/dMRI/ --config ./tempconfig/ --tree ./tempconfig/data_structure.tree --standardspace /path/to/oxford-mm-1_T1_brain.nii.gz -dof 6 -o /path/to/imaging/data/my_multimodal_template/ --cpuq cpu.q --gpuq gpu.q -aff True -nln True -nres 6 -nit 3 --subids ./data/sub_ids.csv --mmorf /path/to/mmorf.sif')
    
    for key in flags_required.keys():
        parser.add_argument(*flags_required[key][0], help=help_required[key], metavar=flags_required[key][1], required=True, nargs='+')
    for key in flags_optional.keys():
        parser.add_argument(*flags_optional[key][0], help=help_optional[key], metavar=flags_optional[key][1], required=False, nargs='*')
    args = parser.parse_args()

    tag = os.path.basename(os.path.abspath(args.output[0]))
    template_dir = args.output[0]
    tree_path = args.tree[0]
    config_dir = args.config[0]
    id_path = args.subids[0]
    df_ids = pd.read_csv(id_path, header=None, names=['subject_ID'], dtype={'subject_ID': str})
    ls_ids = df_ids['subject_ID'].tolist()

    dof = int(args.dof[0])

    affine_on = args.affine[0] == 'True'
    nln_on = args.nonlinear[0] == 'True'

    if args.fslsub_conf is not None:
        os.environ['FSLSUB_CONF'] = args.fslsub_conf[0]

    if nln_on:
        if args.n_resolutions is None or args.n_iterations is None:
            sys.exit('No \'n_resolutions\' or \'n_iterations\' provided')
        else:
            step_id = np.arange(int(args.n_resolutions[0]))+1
            it_at_step_id = np.arange(int(args.n_iterations[0]))+1
        
        if args.cpuq is None or args.gpuq is None:
            sys.exit('No CPU or GPU queue provided')
        else:
            cpuq = args.cpuq[0]
            gpuq = args.gpuq[0]
        
        if args.mmorf is None:
            mmorf_run_cmd = 'mmorf'
        else:
            mmorf_path = args.mmorf[0]
            mmorf_run_cmd = 'singularity run --nv ' + mmorf_path

    if affine_on:
        if args.cpuq is None:
            sys.exit('No CPU queue provided')
        else:
            cpuq = args.cpuq[0]

    standard_space = args.standardspace[0]

    job_ids = ['' for _ in range(100)]
    task_count = 0

    tree = FileTree.read(tree_path, top_level='')
    # if preprocessed_dir is not None:
    #     tree = tree.update(template_dir=template_dir, preprocessed_dir=preprocessed_dir, config_dir=config_dir)
    # else:
    tree = tree.update(template_dir=template_dir, config_dir=config_dir)
    filetree_keys = tree.template_keys()

    n_scalar_mods = len(args.input_scalar)
    scalar_mod_tags = ['scalar_mod_{:02}'.format(i+1) for i in range(n_scalar_mods)]
    tree = tree.update(sub_id='<sub_id>')
    print('\n{} scalar modality/ies will be used. These are located in: '.format(n_scalar_mods))
    for i, mod in enumerate(args.input_scalar):
        tree = tree.update(**{scalar_mod_tags[i]+'_dir': mod})
        print(os.path.dirname(tree.get(scalar_mod_tags[i]+'/scalar_mod_brain')))

    print('\nThe files in {} will be used as the main modality for affine '
          'template construction.'.format(os.path.dirname(tree.get('scalar_mod_01/scalar_mod_brain'))))

    n_tensor_mods = 0
    tensor_mod_tags = []
    if args.input_tensor is not None:
        n_tensor_mods = len(args.input_tensor)
        tensor_mod_tags = ['tensor_mod_{:02}'.format(i+1) for i in range(n_tensor_mods)]
        print('\n{} tensor modality/ies will be used. These are located in:'.format(n_tensor_mods))
        for i, mod in enumerate(args.input_tensor):
            tree = tree.update(**{tensor_mod_tags[0]+'_dir': mod})
            print(os.path.dirname(tree.get(tensor_mod_tags[i]+'/tensor_mod')))

    pre_affine_keys_given = {}
    pre_head_keys_given = {}
    pre_brain_keys_given = {}
    pre_mask_keys_given = {}
    for mod in scalar_mod_tags+tensor_mod_tags:
        pre_affine_keys_given[mod] = False
        pre_head_keys_given[mod] = False
        pre_brain_keys_given[mod] = False
        pre_mask_keys_given[mod] = False
        for key in filetree_keys:
            if mod+'/mod_to_mod_01_mat' in key:
                pre_affine_keys_given[mod] = True
            if mod+'/scalar_mod_head' in key:
                pre_head_keys_given[mod] = True
            if mod+'/scalar_mod_brain' in key:
                pre_brain_keys_given[mod] = True
            if mod+'/scalar_mod_brain_mask' in key:
                pre_mask_keys_given[mod] = True
            if mod+'/tensor_mod_brain_mask' in key:
                pre_mask_keys_given[mod] = True

    print('\n\n\n Checking output folders....')
    os.mkdir(template_dir, mode=0o700) if not os.path.exists(template_dir) else print(template_dir + ' exists')
    script_dir = tree.get('script_dir')
    os.mkdir(script_dir, mode=0o700) if not os.path.exists(script_dir) else print(script_dir + ' exists')
    log_dir = tree.get('log_dir')
    os.mkdir(log_dir, mode=0o700) if not os.path.exists(log_dir) else print(log_dir + ' exists')
    misc_dir = tree.get('misc_dir')
    os.mkdir(misc_dir, mode=0o700) if not os.path.exists(misc_dir) else print(misc_dir + ' exists')
    shutil.copyfile(identity_path,tree.get('identity_mat'))

# Affine template construction
    if affine_on:
# Look for input image with largest FOV and use it as a reference for the first affine iteration
        largest_fov = 0
        aff_ref_id = None
        for id in ls_ids:
            tree = tree.update(sub_id=id, mod=scalar_mod_tags[0])
            img_nib = nib.load(tree.get('scalar_mod_01/scalar_mod_brain'))
            hdr = img_nib.header
            dim = hdr['dim']
            pixdim = hdr['pixdim']
            fov_mm3 = dim[1]*pixdim[1] * dim[2]*pixdim[2] * dim[3]*pixdim[3]
            if fov_mm3 > largest_fov:
                largest_fov = fov_mm3
                aff_ref_id = id
        
        tree = tree.update(sub_id=aff_ref_id, ref_id=aff_ref_id)
        affine_ref_path = tree.get('scalar_mod_01/scalar_mod_brain')

# Register all individual images to one reference image using the first scalar modality
        task_count += 1
        task_name = '{:03d}_affT_registrations_2_ref'.format(task_count)
        script_path = os.path.join(script_dir, task_name + '.sh')
        with open(script_path, 'w+') as f:
            for id in ls_ids:
                tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=scalar_mod_tags[0])

                if id == aff_ref_id:
                    shutil.copyfile(tree.get('identity_mat'), tree.get('mod_to_ref_mat', make_dir=True))
                else:
                    cmd = flirt(tree.get('scalar_mod_01/scalar_mod_brain'), affine_ref_path,
                                omat=tree.get('mod_to_ref_mat', make_dir=True), dof=12, cmdonly=True)
                    cmd = ' '.join(cmd) + '\n'
                    f.write(cmd)
        job_ids[1] = submitJob(script_path, tag+'_'+task_name, log_dir, queue=cpuq, wait_for=None, array_task=True)
        print('\nsubmitted: ' + task_name)

# Unbiasing of the affine transformations
        task_count += 1
        task_name = '{:03d}_affT_correct_bias'.format(task_count)
        aff_matrix_paths = []
        unbiased_matrix_paths = []
        for id in ls_ids:
            tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=scalar_mod_tags[0])
            aff_matrix_paths.append(tree.get('mod_to_ref_mat'))
            unbiased_matrix_paths.append(tree.get('mod_to_unbiased_mat'))

        temp_dir = tree.get('affine_it1_dir')
        unbiasing_invmtx_path = tree.get('mod_unbiasing_affine_matrix', make_dir=True)

        jobcmd = func_to_cmd(correctBiasMidtransWrapper,
                             args=(
                                 aff_matrix_paths, temp_dir, affine_ref_path, unbiasing_invmtx_path, unbiased_matrix_paths),
                             tmp_dir=script_dir,
                             kwargs=None,
                             clean="never")

        job_ids[4] = submitJob(jobcmd, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[1]], array_task=False)
        print('\nsubmitted: ' + task_name)

# Apply unbiased transform to first modality
        task_count += 1
        task_name = '{:03d}_affT_unbiased_transform'.format(task_count)
        script_path = os.path.join(script_dir, task_name + '.sh')
        with open(script_path, 'w+') as f:
            for id in ls_ids:
                tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=scalar_mod_tags[0])
                cmd = applyxfm(src=tree.get(scalar_mod_tags[0]+'/scalar_mod_brain'), ref=affine_ref_path, mat=tree.get('mod_to_unbiased_mat'),
                               out=tree.get('mod_to_unbiased_img'), interp='spline', cmdonly=True)
                cmd = ' '.join(cmd) + '\n'
                f.write(cmd)

        job_ids[5] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[4]], array_task=True)
        print('\nsubmitted: ' + task_name)

# Concatenate the transformations from all other modalities to the first modality with the transformation from the first modality to unbiased space
        task_count += 1
        task_name = '{:03d}_affT_concat_mods_and_unbiased'.format(task_count)
        script_path = os.path.join(script_dir, task_name + '.sh')
        nrows = 0
        with open(script_path, 'w+') as f:
            for mod in pre_affine_keys_given:
                if pre_affine_keys_given[mod]:
                    for id in ls_ids:
                        tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=scalar_mod_tags[0])
                        temp = tree.get('mod_to_unbiased_mat')
                        tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                        cmd = concatxfm(tree.get(mod+'/mod_to_mod_01_mat'), temp,
                                        tree.get('mod_to_unbiased_mat'), cmdonly=True)
                        cmd = ' '.join(cmd) + '\n'
                        f.write(cmd)
                        nrows += 1
                else:
                    for id in ls_ids:
                        tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=scalar_mod_tags[0])
                        temp = tree.get('mod_to_unbiased_mat')
                        tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                        if temp != tree.get('mod_to_unbiased_mat'):
                            cmd = 'cp '+ temp + ' ' + tree.get('mod_to_unbiased_mat') + '\n'
                            f.write(cmd)
                            nrows += 1

        if nrows > 0:
            job_ids[6] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[4]], array_task=True)
            print('\nsubmitted: ' + task_name)

# Averaging unbiased images from the first modality
        task_count += 1
        task_name = '{:03d}_affT_average_unbiased_mod'.format(task_count)
        img_paths = []
        for id in ls_ids:
            tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=scalar_mod_tags[0])
            img_paths.append(tree.get('mod_to_unbiased_img'))
        aff_template_path = tree.get('mod_unbiased_affine_template')

        jobcmd = func_to_cmd(averageImages, args=(img_paths, aff_template_path, None ,'mean', True), tmp_dir=script_dir, kwargs=None,
                             clean="never")
        job_ids[8] = submitJob(jobcmd, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[5]],
                               array_task=False)
        print('\nsubmitted: ' + task_name)

# Register unbiased template to standard space
        task_count += 1
        task_name = '{:03d}_affT_unbiased_mod_to_standardspace'.format(task_count)
        tree = tree.update(mod=scalar_mod_tags[0])
        aff_template_path = tree.get('mod_unbiased_affine_template')
        cmd = flirt(aff_template_path, standard_space, omat=tree.get('mod_unbiased_affine_template_to_standardspace_mat'),
                    out=tree.get('mod_unbiased_affine_template_to_standardspace_img'),
                    searchrx=(-180,180), searchry=(-180,180), searchrz=(-180,180),
                    dof=dof, cmdonly=True)
        cmd = ' '.join(cmd) + '\n'

        job_ids[9] = submitJob(cmd, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[8]],
                            array_task=False)
        print('\nsubmitted: ' + task_name)

# Concatenate individual affine transformations from the modality to unbiased space and from unbiased space to standard space, 
# and apply to corresponding modalities
        task_count += 1
        task_name = '{:03d}_affT_transform_to_standardspace'.format(task_count)
        script_path = os.path.join(script_dir, task_name + '.sh')
        with open(script_path, 'w+') as f:
            cmd = ''
            for id in ls_ids:
                tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=scalar_mod_tags[0])
                temp = tree.get('mod_unbiased_affine_template_to_standardspace_mat')

                for mod in scalar_mod_tags:
                    tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                    cmd_temp = concatxfm(tree.get('mod_to_unbiased_mat'), temp,
                                         tree.get('mod_to_standard_mat', make_dir=True), cmdonly=True)
                    cmd += ' '.join(cmd_temp)

                    if pre_head_keys_given[mod]:
                        cmd_temp = applyxfm(src=tree.get(mod+'/scalar_mod_head'), ref=standard_space, mat=tree.get('mod_to_standard_mat'), 
                                            out=tree.get('mod_to_standard_img'), interp='spline', cmdonly=True)
                        cmd += '; ' + ' '.join(cmd_temp)

                    if pre_brain_keys_given[mod]:
                        cmd_temp = applyxfm(src=tree.get(mod+'/scalar_mod_brain'), ref=standard_space, mat=tree.get('mod_to_standard_mat'),
                                            out=tree.get('mod_to_standard_brain_img'), interp='spline', cmdonly=True)
                        cmd += '; ' + ' '.join(cmd_temp)

                    if pre_mask_keys_given[mod]:
                        cmd_temp = applyxfm(src=tree.get(mod+'/scalar_mod_brain_mask'), ref=standard_space, mat=tree.get('mod_to_standard_mat'),
                                            out=tree.get('mod_to_standard_brain_mask_img'), interp='trilinear', cmdonly=True)
                        cmd += '; ' + ' '.join(cmd_temp)

                    cmd += '\n'

                for mod in tensor_mod_tags:
                    tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                    cmd_temp = concatxfm(tree.get('mod_to_unbiased_mat'), temp,
                                         tree.get('mod_to_standard_mat', make_dir=True), cmdonly=True)
                    cmd += ' '.join(cmd_temp)

                    if pre_mask_keys_given[mod]:
                        cmd_temp = applyxfm(src=tree.get(mod+'/tensor_mod_brain_mask'), ref=standard_space, mat=tree.get('mod_to_standard_mat'),
                                            out=tree.get('mod_to_standard_brain_mask_img'), interp='trilinear', cmdonly=True)
                        cmd += '; ' + ' '.join(cmd_temp)

                    cmd_temp = 'vecreg -i ' + tree.get(mod+'/tensor_mod') + \
                          ' -r ' + standard_space + \
                          ' -o ' + tree.get('mod_to_standard_img') + \
                          ' -t ' + tree.get('mod_to_standard_mat') + \
                          ' --interp=spline'
                    cmd += '; ' + cmd_temp + '\n'
                
            f.write(cmd)

        job_ids[10] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[9]],
                                array_task=True)
        print('\nsubmitted: ' + task_name)

# Averaging transformed images in standard space
        task_count += 1
        task_name = '{:03d}_affT_average_template_in_standardspace'.format(task_count)
        script_path = os.path.join(script_dir, task_name + '.sh')
        with open(script_path, 'w+') as f:
            cmd = ''
            for mod in scalar_mod_tags:
                if pre_head_keys_given[mod]:
                    img_paths = []
                    for id in ls_ids:
                        tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                        img_paths.append(tree.get('mod_to_standard_img'))
                    aff_template_path = tree.get('mod_affine_template')
                    cmd += func_to_cmd(averageImages, args=(img_paths, aff_template_path, None, 'mean', True), tmp_dir=script_dir, 
                                         kwargs=None, clean="always") + '\n'
                    
                if pre_brain_keys_given[mod]:
                    img_paths = []
                    for id in ls_ids:
                        tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                        img_paths.append(tree.get('mod_to_standard_brain_img'))
                    aff_template_path = tree.get('mod_affine_template_brain')
                    cmd += func_to_cmd(averageImages, args=(img_paths, aff_template_path, None, 'mean', True), tmp_dir=script_dir, 
                                         kwargs=None, clean="always") + '\n'
                    
                if pre_mask_keys_given[mod]:
                    img_paths = []
                    for id in ls_ids:
                        tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                        img_paths.append(tree.get('mod_to_standard_brain_mask_img'))
                    aff_template_path = tree.get('mod_affine_template_brain_mask')
                    cmd += func_to_cmd(averageImages, args=(img_paths, aff_template_path, None, 'mean', False), tmp_dir=script_dir, 
                                         kwargs=None, clean="always") + '; '
                    cmd += 'fslmaths ' + aff_template_path + ' -thr 0.1 -bin -mul 7 -add 1 -inm 1 ' + tree.get('mod_affine_template_brain_mask_weighted') + ' \n'
                    
            for mod in tensor_mod_tags:
                img_paths = []
                for id in ls_ids:
                    tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                    img_paths.append(tree.get('mod_to_standard_img'))
                aff_template_path = tree.get('mod_affine_template')
                cmd += func_to_cmd(log_tensor_average, args=(img_paths, aff_template_path), tmp_dir=script_dir, 
                                        kwargs=None, clean="always") + '\n'
                
                if pre_mask_keys_given[mod]:
                    img_paths = []
                    for id in ls_ids:
                        tree = tree.update(sub_id=id, ref_id=aff_ref_id, mod=mod)
                        img_paths.append(tree.get('mod_to_standard_brain_mask_img'))
                    aff_template_path = tree.get('mod_affine_template_brain_mask')
                    cmd += func_to_cmd(averageImages, args=(img_paths, aff_template_path, None, 'mean', False), tmp_dir=script_dir, 
                                         kwargs=None, clean="always") + '; '
                    cmd += 'fslmaths ' + aff_template_path + ' -thr 0.5 ' + tree.get('mod_affine_template_brain_mask_weighted') + ' \n'
                    

            f.write(cmd)

        job_ids[21] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[10]],
                                array_task=True)
        print('\nsubmitted: ' + task_name)

# Nonlinear template construction
    if nln_on:
        it_total = 0

        for s, step in enumerate(step_id):
            tree = tree.update(step_id='{:02d}'.format(step))

            for i, it in enumerate(it_at_step_id):
                print('\nStep ID: ', step)
                print('Iteration ID: ', it)
                it_total += 1
                tree = tree.update(step_id='{:02d}'.format(step), it_id='{:02d}'.format(it))

                mmorf_args_ref = {'mmorf_run_cmd': '', 'config_path': '', 'img_warp_space': '',
                                  'img_ref_scalar': [], 'aff_ref_scalar': [], 'mask_ref_scalar': [],
                                  'img_ref_tensor': [], 'aff_ref_tensor': [], 'mask_ref_tensor': []}

                mmorf_args_ref['mmorf_run_cmd'] = mmorf_run_cmd
                shutil.copyfile(tree.get('mmorf_params_{:02d}'.format(step)), tree.get('mmorf_params', make_dir=True))
                mmorf_args_ref['config_path'] = tree.get('mmorf_params')

                if it_total == 1:
                    tree = tree.update(step_id='{:02d}'.format(step), it_id='{:02d}'.format(it))

                    for mod in scalar_mod_tags:
                        tree = tree.update(mod=mod)
                        if pre_head_keys_given[mod]:
                            mmorf_args_ref['img_ref_scalar'].append(tree.get('mod_affine_template'))
                            mmorf_args_ref['aff_ref_scalar'].append(tree.get('identity_mat'))

                            if pre_mask_keys_given[mod]:
                                mmorf_args_ref['mask_ref_scalar'].append(tree.get('mod_affine_template_brain_mask_weighted'))
                            else:
                                mmorf_args_ref['mask_ref_scalar'].append('NULL')

                    if not any(list(pre_head_keys_given.values())):
                        for mod in scalar_mod_tags:
                            tree = tree.update(mod=mod)
                            if pre_brain_keys_given[mod]:
                                mmorf_args_ref['img_ref_scalar'].append(tree.get('mod_affine_template_brain'))
                                mmorf_args_ref['aff_ref_scalar'].append(tree.get('identity_mat'))

                                if pre_mask_keys_given[mod]:
                                    mmorf_args_ref['mask_ref_scalar'].append(tree.get('mod_affine_template_brain_mask_weighted'))
                                else:
                                    mmorf_args_ref['mask_ref_scalar'].append('NULL')

                    mmorf_args_ref['img_warp_space'] = mmorf_args_ref['img_ref_scalar'][0]

                    for mod in tensor_mod_tags:
                        tree = tree.update(mod=mod)
                        mmorf_args_ref['img_ref_tensor'].append(tree.get('mod_affine_template'))
                        mmorf_args_ref['aff_ref_tensor'].append(tree.get('identity_mat'))

                        if pre_mask_keys_given[mod]:
                            mmorf_args_ref['mask_ref_tensor'].append(tree.get('mod_affine_template_brain_mask_weighted'))
                        else:
                            mmorf_args_ref['mask_ref_tensor'].append('NULL')

                    tree = tree.update(mod=scalar_mod_tags[0])
                    avgwarp_prev_it_path = tree.get('zero_warp_affine')
                    avgmask_prev_it_path = tree.get('mod_affine_template')

                else:
                    if it == 1:
                        prev_it = it_at_step_id[-1]
                        prev_step = step - 1
                    elif it > 1:
                        prev_it = it - 1
                        prev_step = step

                    tree = tree.update(step_id='{:02d}'.format(prev_step), it_id='{:02d}'.format(prev_it))

                    for mod in scalar_mod_tags:
                        tree = tree.update(mod=mod)
                        if pre_head_keys_given[mod]:
                            mmorf_args_ref['img_ref_scalar'].append(tree.get('mod_nonlinear_template'))
                            mmorf_args_ref['aff_ref_scalar'].append(tree.get('identity_mat'))

                            if pre_mask_keys_given[mod]:
                                mmorf_args_ref['mask_ref_scalar'].append(tree.get('mod_nonlinear_template_brain_mask_weighted'))
                            else:
                                mmorf_args_ref['mask_ref_scalar'].append('NULL')
                    
                    if not any(list(pre_head_keys_given.values())):
                        for mod in scalar_mod_tags:
                            tree = tree.update(mod=mod)
                            if pre_brain_keys_given[mod]:
                                mmorf_args_ref['img_ref_scalar'].append(tree.get('mod_nonlinear_template_brain'))
                                mmorf_args_ref['aff_ref_scalar'].append(tree.get('identity_mat'))

                                if pre_mask_keys_given[mod]:
                                    mmorf_args_ref['mask_ref_scalar'].append(tree.get('mod_nonlinear_template_brain_mask_weighted'))
                                else:
                                    mmorf_args_ref['mask_ref_scalar'].append('NULL')

                    mmorf_args_ref['img_warp_space'] = mmorf_args_ref['img_ref_scalar'][0]

                    for mod in tensor_mod_tags:
                        tree = tree.update(mod=mod)
                        mmorf_args_ref['img_ref_tensor'].append(tree.get('mod_nonlinear_template'))
                        mmorf_args_ref['aff_ref_tensor'].append(tree.get('identity_mat'))

                        if pre_mask_keys_given[mod]:
                            mmorf_args_ref['mask_ref_tensor'].append(tree.get('mod_nonlinear_template_brain_mask_weighted'))
                        else:
                            mmorf_args_ref['mask_ref_tensor'].append('NULL')

                    avgwarp_prev_it_path = tree.get('avg_warp')
                    tree = tree.update(step_id='{:02d}'.format(prev_step), it_id='{:02d}'.format(prev_it), mod=scalar_mod_tags[0])
                    avgmask_prev_it_path = tree.get('mod_nonlinear_template_brain_mask')

                    tree = tree.update(step_id='{:02d}'.format(step), it_id='{:02d}'.format(it))

# Nonlinear registration to template from previous iteration
                task_count += 1
                task_name = '{:03d}_nlnT_mmorf'.format(task_count)
                script_path = os.path.join(script_dir, task_name + '.sh')
                with open(script_path, 'w+') as f:
                    export_vars = {}
                    for i, id in enumerate(ls_ids):
                        mmorf_args_mov = {'img_mov_scalar': [], 'aff_mov_scalar': [], 'mask_mov_scalar': [],
                                      'img_mov_tensor': [], 'aff_mov_tensor': [], 'mask_mov_tensor': [],
                                      'warp_out': '', 'jac_det_out': '', 'bias_out': ''}
                                        
                        for mod in scalar_mod_tags:
                            tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod)
                            if pre_head_keys_given[mod]:
                                mmorf_args_mov['img_mov_scalar'].append(tree.get(mod+'/scalar_mod_head'))
                                mmorf_args_mov['aff_mov_scalar'].append(tree.get('mod_to_standard_mat'))

                                if pre_mask_keys_given[mod]:
                                    mmorf_args_mov['mask_mov_scalar'].append(tree.get(mod+'/scalar_mod_brain_mask'))
                                else:
                                    mmorf_args_mov['mask_mov_scalar'].append('NULL')

                        if not any(list(pre_head_keys_given.values())):
                            for mod in scalar_mod_tags:
                                tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod)
                                if pre_brain_keys_given[mod]:
                                    mmorf_args_mov['img_mov_scalar'].append(tree.get(mod+'/scalar_mod_brain'))
                                    mmorf_args_mov['aff_mov_scalar'].append(tree.get('mod_to_standard_mat'))

                                    if pre_mask_keys_given[mod]:
                                        mmorf_args_mov['mask_mov_scalar'].append(tree.get(mod+'/scalar_mod_brain_mask'))
                                    else:
                                        mmorf_args_mov['mask_mov_scalar'].append('NULL')

                        for mod in tensor_mod_tags:
                            tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod)
                            mmorf_args_mov['img_mov_tensor'].append(tree.get(mod+'/tensor_mod'))
                            mmorf_args_mov['aff_mov_tensor'].append(tree.get('mod_to_standard_mat'))

                            if pre_mask_keys_given[mod]:
                                mmorf_args_mov['mask_mov_tensor'].append(tree.get(mod+'/tensor_mod_brain_mask'))
                            else:
                                mmorf_args_mov['mask_mov_tensor'].append('NULL')

                        mmorf_args_mov['warp_out'] = tree.get('mmorf_warp', make_dir=True)
                        mmorf_args_mov['jac_det_out'] = tree.get('mmorf_jac')
                        mmorf_args_mov['bias_out'] = tree.get('mmorf_bias')

                        mmorf_args = {**mmorf_args_ref, **mmorf_args_mov}
                        mmorf_script, export_var = mmorfWrapper(**mmorf_args)
                        f.write(mmorf_script)

                        for key, value in export_var.items():
                            if i == 0:
                                export_vars[key] = value
                            else:
                                export_vars[key] = export_vars[key] + value

                    export_var_str = {}
                    for key, value in export_vars.items():
                        common_path = os.path.commonpath(value)
                        export_var_str[key] = '"' + key + '=' + ','.join([common_path]) + '"'

                job_ids[28] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=gpuq,
                                        wait_for=list(itemgetter(*[21, 34])(job_ids)),
                                        array_task=True, coprocessor='cuda', coprocessor_class=None, coprocessor_multi="1", threads=1, export_var=[export_var_str['SINGULARITY_BIND']])
                print('\nsubmitted: ' + task_name)

                ref = mmorf_args_ref['img_warp_space']

# Averaging warps
                task_count += 1
                task_name = '{:03d}_nlnT_average_warps'.format(task_count)
                warp_paths = []
                for id in ls_ids:
                    tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=scalar_mod_tags[0])
                    warp_paths.append(tree.get('mmorf_warp'))
                avg_warp_path = tree.get('avg_warp')

                jobcmd = func_to_cmd(averageImages, args=(warp_paths, avg_warp_path, None, 'mean', False), tmp_dir=script_dir,
                                     kwargs=None, clean="always") + '; '

                avg_warp_path = tree.get('avg_warp')
                inv_avg_warp_path = tree.get('inv_avg_warp')
                cmd = invwarp(warp=avg_warp_path, ref=ref, out=inv_avg_warp_path, cmdonly=True)
                jobcmd += ' '.join(cmd) + '\n'

                job_ids[29] = submitJob(jobcmd, tag + '_' + task_name, log_dir, queue=cpuq,wait_for=[job_ids[28]],
                                        array_task=False)
                print('\nsubmitted: ' + task_name)

# # Not in use! This warp-composition step was integrated into the next step.
# # Create unbiased warps: (1) resample forward warp with inverse average warp and (2) add inverse average warp to resulting composition
#                 task_count += 1
#                 task_name = '{:03d}_nlnT_resample_warps'.format(task_count)
#                 script_path = os.path.join(script_dir, task_name + '.sh')
#                 with open(script_path, 'w') as f:
#                     cmd = ''
#                     for id in ls_ids:
#                         tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=scalar_mod_tags[0])
#                         warp_path = tree.get('mmorf_warp')
#                         resampled_path = tree.get('mmorf_warp_resampled')
#                         inv_avg_warp_path = tree.get('inv_avg_warp')

#                         cmd += func_to_cmd(applyWarpWrapper, args=(warp_path, ref, resampled_path, inv_avg_warp_path, 'spline', False, None),
#                                     tmp_dir=script_dir, kwargs=None, clean="always") + '; '
#                         cmd += 'fslmaths ' + tree.get('mmorf_warp_resampled') + ' -add ' + tree.get('inv_avg_warp') + ' ' + tree.get('mmorf_warp_resampled_unbiased') + '\n'
                    
#                     f.write(cmd)

#                 job_ids[32] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[29]],
#                                         array_task=True)
#                 print('\nsubmitted: ' + task_name)

# Concatenate corresponding affine transforms and unbiased warps, and apply to corresponding images
                task_count += 1
                task_name = '{:03d}_nlnT_concat_unbiased_warps_mods'.format(task_count)
                script_path = os.path.join(script_dir, task_name + '.sh')

                with open(script_path, 'w+') as f:
                    for id in ls_ids:
                        tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=scalar_mod_tags[0])
                        for mod in scalar_mod_tags:
                            tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod)
                            # cmd = convertwarp(out=tree.get('mmorf_warp_resampled_unbiased_full_mod'),
                            #                   ref=ref,
                            #                   premat=tree.get('mod_to_standard_mat'),
                            #                   warp1=tree.get('mmorf_warp_resampled_unbiased'),
                            #                   relout=True, cmdonly=True)
                            cmd = convertwarp(out=tree.get('mmorf_warp_resampled_unbiased_full_mod'),
                                              ref=ref,
                                              premat=tree.get('mod_to_standard_mat'),
                                              warp1=tree.get('mmorf_warp'),
                                              warp2=tree.get('inv_avg_warp'),
                                              relout=True, cmdonly=True)
                            cmd = ' '.join(cmd)

                            if pre_head_keys_given[mod]:
                                cmd += '; ' + func_to_cmd(applyWarpWrapper, args=(tree.get(mod+'/scalar_mod_head'),
                                                                                  ref,
                                                                                  tree.get('warped_mod'),
                                                                                  tree.get('mmorf_warp_resampled_unbiased_full_mod'),
                                                                                  'spline', False, True),
                                                                                  tmp_dir=script_dir, kwargs=None, clean="always")
                            if pre_brain_keys_given[mod]:
                                cmd += '; ' + func_to_cmd(applyWarpWrapper, args=(tree.get(mod+'/scalar_mod_brain'),
                                                                                  ref,
                                                                                  tree.get('warped_mod_brain'),
                                                                                  tree.get('mmorf_warp_resampled_unbiased_full_mod'),
                                                                                  'spline', False, True),
                                                                                  tmp_dir=script_dir, kwargs=None, clean="always")
                            if pre_mask_keys_given[mod]:
                                cmd += '; ' + func_to_cmd(applyWarpWrapper, args=(tree.get(mod+'/scalar_mod_brain_mask'),
                                                                                  ref,
                                                                                  tree.get('warped_mod_brain_mask'),
                                                                                  tree.get('mmorf_warp_resampled_unbiased_full_mod'),
                                                                                  'trilinear', False, True),
                                                                                  tmp_dir=script_dir, kwargs=None, clean="always")
                            cmd += '\n'
                            f.write(cmd)

                        for mod in tensor_mod_tags:
                            tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod)
                            cmd = convertwarp(out=tree.get('mmorf_warp_resampled_unbiased_full_mod'),
                                              ref=ref,
                                              premat=tree.get('mod_to_standard_mat'),
                                              warp1=tree.get('mmorf_warp'),
                                              warp2=tree.get('inv_avg_warp'),
                                              relout=True, cmdonly=True)
                            cmd = ' '.join(cmd)

                            cmd += '; ' + 'vecreg -i ' + tree.get(mod+'/tensor_mod') + \
                                      ' -r ' + ref + \
                                      ' -o ' + tree.get('warped_mod') + \
                                      ' -w ' + tree.get('mmorf_warp_resampled_unbiased_full_mod') + \
                                      ' --interp=spline'
                            
                            if pre_mask_keys_given[mod]:
                                cmd += '; ' + func_to_cmd(applyWarpWrapper, args=(tree.get(mod+'/tensor_mod_brain_mask'),
                                                                                  ref,
                                                                                  tree.get('warped_mod_brain_mask'),
                                                                                  tree.get('mmorf_warp_resampled_unbiased_full_mod'),
                                                                                  'trilinear', False, True),
                                                                                  tmp_dir=script_dir, kwargs=None, clean="always")
                            cmd += '\n'
                            f.write(cmd)

                job_ids[33] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[29]],
                                        array_task=True)
                print('\nsubmitted: ' + task_name)

# Averaging transformed images in template space
                task_count += 1
                task_name = '{:03d}_nlnT_average_modalities'.format(task_count)
                script_path = os.path.join(script_dir, task_name + '.sh')
                with open(script_path, 'w+') as f:
                    cmd = ''
                    for mod_id, mod in enumerate(scalar_mod_tags):
                        if pre_head_keys_given[mod]:
                            img_paths = []
                            biasfield_paths = []
                            for id in ls_ids:
                                tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod, mod_id=mod_id)
                                img_paths.append(tree.get('warped_mod'))
                                biasfield_paths.append(tree.get('mmorf_bias_mod'))
                            template_path = tree.get('mod_nonlinear_template')
                            cmd += func_to_cmd(averageImages, args=(img_paths, template_path, biasfield_paths, 'mean', True), tmp_dir=script_dir, 
                                                kwargs=None, clean="always") + '\n'
                            
                        if pre_brain_keys_given[mod]:
                            img_paths = []
                            biasfield_paths = []
                            for id in ls_ids:
                                tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod, mod_id=mod_id)
                                img_paths.append(tree.get('warped_mod_brain'))
                                biasfield_paths.append(tree.get('mmorf_bias_mod'))
                            template_path = tree.get('mod_nonlinear_template_brain')
                            cmd += func_to_cmd(averageImages, args=(img_paths, template_path, biasfield_paths, 'mean', True), tmp_dir=script_dir, 
                                                kwargs=None, clean="always") + '\n'
                            
                        if pre_mask_keys_given[mod]:
                            img_paths = []
                            for id in ls_ids:
                                tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod)
                                img_paths.append(tree.get('warped_mod_brain_mask'))
                            template_path = tree.get('mod_nonlinear_template_brain_mask')
                            cmd += func_to_cmd(averageImages, args=(img_paths, template_path, None, 'mean', False), tmp_dir=script_dir, 
                                                kwargs=None, clean="always") + '; '
                            cmd += 'fslmaths ' + template_path + ' -thr 0.1 -bin -mul 7 -add 1 -inm 1 ' + tree.get('mod_nonlinear_template_brain_mask_weighted') + ' \n'
                            
                    for mod in tensor_mod_tags:
                        img_paths = []
                        for id in ls_ids:
                            tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod)
                            img_paths.append(tree.get('warped_mod'))
                        template_path = tree.get('mod_nonlinear_template')
                        cmd += func_to_cmd(log_tensor_average, args=(img_paths, template_path), tmp_dir=script_dir, 
                                                kwargs=None, clean="always") + '\n'
                        
                        if pre_mask_keys_given[mod]:
                            img_paths = []
                            for id in ls_ids:
                                tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=mod)
                                img_paths.append(tree.get('warped_mod_brain_mask'))
                            template_path = tree.get('mod_nonlinear_template_brain_mask')
                            cmd += func_to_cmd(averageImages, args=(img_paths, template_path, None, 'mean', False), tmp_dir=script_dir, 
                                                kwargs=None, clean="always") + '; '
                            cmd += 'fslmaths ' + template_path + ' -thr 0.5 ' + tree.get('mod_nonlinear_template_brain_mask_weighted') + ' \n'
                            
                    f.write(cmd)
                
                job_ids[34] = submitJob(script_path, tag + '_' + task_name, log_dir, queue=cpuq, wait_for=[job_ids[33]],
                                        array_task=True)
                print('\nsubmitted: ' + task_name)

    # # Convergence monitoring
    # # Difference in average warp between consecutive iterations
    #             if it_total > 1:
    #                 task_count += 1
    #                 task_name = '{:03d}_nlnT_average_warp_diff'.format(task_count)
    #                 tree = tree.update(step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=scalar_mod_tags[0])
    #                 avgwarp_curr_it_path = tree.get('avg_warp')
    #                 avgmask_curr_it_path = tree.get('mod_nonlinear_template_brain_mask')
    #                 jobcmd = func_to_cmd(RMSdifference, args=(
    #                 avgwarp_curr_it_path, avgwarp_prev_it_path, avgmask_curr_it_path, avgmask_prev_it_path,
    #                 tree.get('delta_avgwarp_output')),
    #                                         tmp_dir=script_dir, kwargs=None,
    #                                         clean="always")

    #                 job_ids[51] = submitJob(jobcmd, tag + '_' + task_name, log_dir, queue=cpuq,
    #                                         wait_for=list(itemgetter(*[34])(job_ids)), array_task=False)
    #                 print('submitted: ' + task_name)

    # # Difference in template intensity between consecutive iterations
    #             if it_total > 1:
    #                 task_count += 1
    #                 task_name = '{:03d}_nlnT_temp_intensity_diff'.format(task_count)
    #                 tree = tree.update(step_id='{:02d}'.format(step), it_id='{:02d}'.format(it), mod=scalar_mod_tags[0])
    #                 nln_template_curr_it_path = tree.get('mod_nonlinear_template')
    #                 avgmask_curr_it_path = tree.get('mod_nonlinear_template_brain_mask')
    #                 jobcmd = func_to_cmd(RMSdifference, args=(
    #                 nln_template_curr_it_path, mmorf_args_ref['img_ref_scalar'], avgmask_curr_it_path, avgmask_prev_it_path,
    #                 tree.get('delta_intensity_output')),
    #                                         tmp_dir=script_dir, kwargs=None,
    #                                         clean="never")

    #                 job_ids[52] = submitJob(jobcmd, tag + '_' + task_name, log_dir, queue=cpuq,
    #                                         wait_for=list(itemgetter(*[34])(job_ids)), array_task=False)
    #                 print('submitted: ' + task_name)

    # # RMS of intensity standard deviation of individual images in template space
    #             task_count += 1
    #             task_name = '{:03d}_nlnT_temp_intensity_sd'.format(task_count)
    #             img_paths = []
    #             for i, id in enumerate(ls_ids):
    #                 tree = tree.update(sub_id=id, step_id='{:02d}'.format(step), it_id='{:02d}'.format(it))
    #                 img_paths.append(tree.get('warped_T1brain'))

    #             jobcmd = func_to_cmd(RMSstandardDeviation, args=(img_paths,
    #                                                                 tree.get('T1_head_nln_template'),
    #                                                                 tree.get('T1_brain_mask_nln_template'),
    #                                                                 tree.get('T1_head_SD_nln_template'),
    #                                                                 tree.get('sd_intensity_output')),
    #                                     tmp_dir=script_dir, kwargs=None,
    #                                     clean="never")

    #             job_ids[53] = submitJob(jobcmd, tag + '_' + task_name, log_dir, queue=cpuq,
    #                                     wait_for=list(itemgetter(*[45, 49])(job_ids)), array_task=False)
    #             print('submitted: ' + task_name)





















