# Self-consistent multimodal template construction
# Version 1
This version allows the construction of a self-consistent multimodal template by jointly informing the construction process with scalar and tensor modalities. This version has the advantage of allowing an arbitrary number of scalar and tensor modalities to be included (in contrast to version 0, which was limited to combinations of T1, T2, and DTI.)

## Setup and requirements

- Jobs are submitted to the cluster with `fsl_sub` which supports SGE and Slurm scheduler. Depending on the system, fsl_sub_plugin_sge or fsl_sub_plugin_slurm can be installed with the conda command shown below.
- The latest version of FSL - more information on how to install it can be found on https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation.
- Create a Conda environment and make sure the latest versions of the following packages are installed. Development and testing was done with Python 3.9 and a Slurm job scheduler. Installation can be performed with:

```bash
conda create -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ -c conda-forge --prefix ./template_env python=3.9 pandas numpy nibabel fslpy fsl-sub file-tree-fsl
conda activate ./template_env
```
and depending on the workload manager either:
```
conda install -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ -c conda-forge fsl-sub-plugin-slurm
```
or
```
conda install -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ -c conda-forge fsl-sub-plugin-sge
```

- MMORF installation
    - Either use an MMORF Singularity image, or install MMORF via conda
    - MMORF Singularity container for the GPU-based nonlinear registrations can be downloaded from https://git.fmrib.ox.ac.uk/flange/mmorf_beta.
    - MMORF conda installation can be performed by adding the following line to the command above:

```bash
conda install -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/ -c conda-forge fsl_mmorf-cuda-10.2
```

## Running the template construction pipeline
Cloning this repository will provide you with the main script for constructing a template (`run_template_construction.py`), and a directory with template config files (`tempconfig`).
These config files contain information about the directory structure of the input and output data (`data_structure.tree`) with one file per input modality (e.g. `scalar_mod_T1.tree`, `scalar_mod_FLAIR.tree`) allowing for maximum flexibility. It also contains config files containing the nonlinear registration parameters (`mmorf_params_step<0..6>.ini`). In the next paragraphs I will present examples of how these files can be adjusted to the user's data for the pipeline to function correctly.

The repository can be downloaded/cloned and run with the following commands (do make sure that the environment from the previous step is activated):
```bash
git clone https://git.fmrib.ox.ac.uk/cart/mm-template-construction.git
cd mm-template-construction/template_construction_v1

python run_template_construction.py [Options]
```

### Preprocessing and preparing the data
Preprocessing steps should be performed before running the pipeline. This includes bias field correction with FSL FAST and skull-stripping (=brain extraction) with FSL BET or comparable tools. The brain-extracted images from the first scalar modality will be used for affine template construction. To make sure that all modalities of a subject are in the same space, they have to be registered to the first scalar modality within-subject e.g. FLAIR and DTI rigidly registered to the corresponding T1 of a subject. The registered images can then be directly used for template construction or, alternatively, the acquired transformations can be provided to the template construction process to avoid interpolation steps. In case of the latter, the transformations will be automatically composed with all the transformations and deformations estimated during template construction. Other potential preprocessing steps that can improve template construction include the capping of unusually high intensities.

For the pipeline to know which modalities to use and where the images are located, the data structure has to be specified in the filetree `data_structure.tree` (more information about filetrees can be found on https://open.win.ox.ac.uk/pages/ndcn0236/file-tree/) and the modality-specific filetrees (e.g. scalar_mod_T1). This filetree can contain as many placeholders for scalar and tensor modalities as desired with a minimum of one scalar modality. The default settings in the provided filetree contain three placeholders for three input modalities, i.e. scalar_mod_01_dir, scalar_mod_02_dir and tensor_mod_01_dir, in this case two scalar and one tensor modality respectively. Each of the associated links points to a separate filetree that describes the specific data structure used to store the corresponding modality - in this case T1, FLAIR and DTI. The number of placeholders can be adjusted by commenting out/deleting or adding new blocks as will be illustrated in the next examples.

The first scalar modality will be used for affine template construction i.e. `->scalar_mod_T1` links to the filetree for T1 images, which will produce an affine template based on T1 images. However, all of the modalities will be used in the nonlinear template construction. Looking into scalar_mod_T1 we can see a placeholder for the subject ID `{sub_id}` and several image filenames with their tags in brackets. The placeholders will later be automatically filled in with the provided data. Filenames and directory hierarchies can be changed according to the actual relative paths since the template construction script only refers to the tags (hence, these must not be changed):

```bash
{sub_id} (scalar_sub_dir)
    T1 (scalar_mod_dir)
        T1_unbiased{ext_nii} (scalar_mod_head)
        T1_unbiased_brain{ext_nii} (scalar_mod_brain)
        T1_brain_mask{ext_nii} (scalar_mod_brain_mask)
```

describes a file structure looking similar to:

```bash
    + subject_1
            + T1
                |-- T1_unbiased.nii.gz
                |-- T1_unbiased_brain.nii.gz
                |-- T1_brain_mask.nii.gz
    .
    .
    .
    + subject_10
            + T1
                |-- T1_unbiased.nii.gz
                |-- T1_unbiased_brain.nii.gz
                |-- T1_brain_mask.nii.gz
```

- `scalar_mod_brain`: the first scalar modality is required to have a brain-extracted image which is used for affine template construction. A brain-extracted image can also be provided for other scalar modalities to create an average brain-extracted volume but will not be used to inform the construction process if a whole-head image is provided.
- `scalar_mod_head`: a whole-head image is optional - if provided, it will be used for the nonlinear template construction instead of the brain-extracted image
- `scalar_mod_brain_mask`: a mask is optional - if provided, it can be used to apply different regularization e.g. intra- vs. extracranial. Whether this is used or not also depends on the parameter settings in the MMORF config.
- `mod_to_mod_01_mat`: an affine transform can be provided for all modalities except the first modality - for example, this could be the affine transformation matrix from FLAIR (2nd modality) to the corresponding T1 (1st modality) of the same subject. These affine transformations will be internally composed with the warps and thereby multiple interpolations can be avoided when creating the template. If provided, `scalar_mod_brain`, `scalar_mod_head`, `scalar_mod_brain_mask` have to be in native space rather than the already registered, e.g. T1, space.

!!! The filenames, e.g. T1_unbiased.nii.gz, and directory hierarchies can be changed, but the names of the tags, i.e. scalar_mod_head, scalar_mod_brain, scalar_mod_brain_mask, mod_to_mod_01_mat must not be modified if the corresponding file should be used !!!

The second scalar modality we would like to use in this example is FLAIR, with `->scalar_mod_FLAIR` linking to the filetree for FLAIR images. Similar to above we can provide a whole-head image with the tag `scalar_mod_head`, a brain-extracted image with the tag `scalar_mod_brain` and a mask with the tag `scalar_mod_brain_mask`. In this example we have decided not to use a mask for FLAIR, and the whole-head image will be used in the nonlinear template construction process.

If we wanted to we could add more scalar modalities by adding another block with a link to a corresponding filetree, for example, a third modality for T2 images:

```bash
!{scalar_mod_03_dir}
    ->scalar_mod_T2 (scalar_mod_03)
```
with a link to a T2 filetree:

```bash
{sub_id} (scalar_sub_dir)
    T2 (scalar_mod_dir)
        T2_unbiased{ext_nii} (scalar_mod_head)
```

Finally, in this example we have also added one tensor modality i.e. `tensor_mod_01` linking to the `->tensor_mod_DTI` filetree:

```bash
{sub_id} (tensor_sub_dir)
    dMRI (tensor_mod_dir)
        dMRI (tensor_mod_subdir)
            dti_tensor{ext_nii} (tensor_mod)
            nodif_brain_mask_ud{ext_nii} (tensor_mod_brain_mask)
    fieldmap
        fieldmap_iout_to_T1{ext_mat} (mod_to_mod_01_mat)
```

For tensor modalities we have three possible tags:
- `tensor_mod`: the DTI tensor (=tensor fitted to the dMRI data) must be provided for this type of modality
- `tensor_mod_brain_mask`: a brain mask is optional and depending on the mask values can be used to e.g. further reduce the impact of noise around the brain border
- `mod_to_mod_01_mat`: an affine transformation from the DTI space to the corresponding first scalar modality e.g. T1 can be provided to avoid interpolations. If provided, `tensor_mod` and `tensor_mod_brain_mask` have to be in native space rather than the already registered space.


The parameters for the nonlinear registrations at each resolution level are described in the files of config_dir. This repository already includes a basic set of parameter files in the folder 'tempconfig' which can be pointed to:
```bash
!{config_dir}
    mmorf_params_step01{ext_ini} (mmorf_params_01)
    mmorf_params_step02{ext_ini} (mmorf_params_02)
    mmorf_params_step03{ext_ini} (mmorf_params_03)
    mmorf_params_step04{ext_ini} (mmorf_params_04)
    mmorf_params_step05{ext_ini} (mmorf_params_05)
    mmorf_params_step06{ext_ini} (mmorf_params_06)
```

All output will be written into the directory assigned to the placeholder `!{template_dir}`. Do not change anything in this part of the filetree.

All placeholders starting with an exclamation mark such as `!{scalar_mod_01_dir}`, `!{config_dir}` and `!{template_dir}` will be replaced with directory paths provided by the following arguments. Similarly, the subject ID placeholders will be replaced with the subject IDs provided with the arguments below.

### Options
        'input_scalar': [('-input_scalar','--input_scalar'),'<dir> <dir> ...'],
        'config': [('-config','--config'),'<dir>'],
        'tree': [('-tree','--tree'), '<path>'],
        'subids': [('-subids','--subids'),'<path>'],
        'output': [('-o', '--output'),'<dir>'],
        'affine': [('-aff', '--affine'), '[True,False]'],
        'dof': [('-dof', '--dof'), '[6,12]']
        'nonlinear': [('-nln', '--nonlinear'), '[True,False]'],
        'n_resolutions': [('-nres', '--n_resolutions'), '<int>'],
        'n_iterations': [('-nit', '--n_iterations'), '<int>'],
        'standardspace': [('-stdspace', '--standardspace'),'<path>'],
        'cpuq': [('-cpuq','--cpuq'), '<string>'],
        'gpuq': [('-gpuq','--gpuq'), '<string>'],
        'input_tensor': [('-input_tensor','--input_tensor'),'<dir> <dir> ...'],
        'mmorf': [('-m', '--mmorf'), '<path>']

####   `-h, --help`
Show help message

####   `-input_scalar <dir> <dir> ..., --input_scalar <dir> <dir> ...`
Absolute paths to directories containing the scalar subject/timepoint scalar images. One path per modality is required since each modality could be stored in a different location.

####   `-input_tensor <dir> <dir> ..., --input_tensor <dir> <dir> ...`
Absolute paths to directories containing the scalar subject/timepoint tensor images. One path per modality is required since each modality could be stored in a different location.

####   `-config <dir>, --config <dir>`
Absolute path to directory containing the mmorf_params_step<0..6>.ini files e.g. /path/to/tempconfig/

####  `-standarspace <dir>, --standarspace <dir>`
Path to a standard reference space e.g. OMM-1 or MNI space. The reference template selected here should be of the same modality as the first scalar modality specified in the filetree. The first modality of the brain-extracted affine template will be rigidly registered to this standard space. So for the multimodal template to be in OMM-1 space, you could provide the path to the brain-extracted OMM-1 volume of the modality specified as the first scalar modality in the filetree e.g. T1.

####   `-dof [6,12], --dof [6,12]`
Number of degrees of freedom used for registering the affine template to the standard space (6 DOF=rigid registration, 12 DOF=affine registration).

####   `-m <path>, --mmorf <path>`
Absolute path to mmorf.sif

####   `-tree <path>, --tree <path>`
Path to FSL Filetree describing the subject-specific directory structure and selection of modalities. The example file-tree is located in the folder `tempconfig` and can be edited as described above.

`{sub_id}` is a placeholder and will be automatically replaced with IDs from the CSV file provided with `--subids <path>`

####   `-o <dir>, --output <dir>`
Absolute path to output directory. All temporary files and templates will be saved to this directory.

####   `-subids <path>, --subids <path>`
Path to .csv file containing one subject ID per row: subject IDs have to indentify the sub-directories of the 'input' argument. In the example above these would be subject_1, subject_2, etc.

####   `-aff [True,False], --affine [True,False]`
Runs affine template construction if True.

####   `-nln [True,False], --nonlinear [True,False]`
Runs nonlinear template construction if True

####   `-cpuq <string>, --cpuq <string>`
Name of a cluster queue to submit CPU jobs to (required for affine and nonlinear template construction)

####   `-gpuq <string>, --gpuq <string>`
Name of a cluster queue to submit GPU jobs to (required for nonlinear template construction)

####   `-nres <int>, --n_resolutions <int>`
Number of resolution levels (required for nonlinear template construction)

####   `-nit <int>, --n_iterations <int>`
Number of iterations per resolution level (required for nonlinear template construction)

### Example
A typical command for multimodal template construction with T1, FLAIR and DTI modalities could look like this:
```bash
python run_template_construction.py \
--input_scalar /path/to/imaging/data/subjectsAll/T1/ /path/to/imaging/data/subjectsAll/FLAIR/ \
--input_tensor /path/to/imaging/data/subjectsAll/dMRI/ \
--config ./tempconfig/ \
--tree ./tempconfig/data_structure.tree \
--standardspace /path/to/oxford-mm-1_T1_brain.nii.gz \
--dof 6 \
-o /path/to/imaging/data/my_multimodal_template/ \
--cpuq cpu.q --gpuq gpu.q \
-aff True -nln True \
-nres 6 -nit 3 \
--subids ./data/sub_ids.csv \
--mmorf /path/to/mmorf.sif
```

## Output
Output directories will be automatically created for different purposes. These include:
 
####    `log`
Contains log-files from the job submission system; this is very useful for debugging if there is an error

###     `scripts`
Contains generated Bash and Python scripts e.g. for job array submissions, wrappers for Python functions

####    `misc`
Contains useful files like an identity transform

####    `affine_step1`
Contains the output of the first set of rigd and affine registrations including 
- unbiasing transformation removing the bias introduced by the reference subject
- unbiased average template of the first scalar modality in the unbiased space of the subjects
- rigid transformation of unbiased template to standard space

####    `affine_step2`
Contains the transformed images in standard space and the affine volumes of the template

####    `nln_step_<0..n_res>/it_<0...n_it>`
Contains the warps, bias fields, Jacobians and transformed images as well as the template generated at the corresponding resolution level.


# Version 0
## Setup and requirements

- Create a Conda environment and make sure the latest versions of the following packages are installed. Development and testing
was done with Python 3.7. Installation can be performed with:

```bash
conda create -n tempy_env python=3.7 pandas numpy
conda activate tempy_env
conda install -c conda-forge nibabel
conda install -c conda-forge fslpy
pip install file-tree file-tree-fsl
```
and depending on the cluster management system (SGE or Slurm) the corresponding `fsl_sub` version is required:

```bash
conda install -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/channel fsl_sub_plugin_sge
conda install -c https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/channel fsl_sub_plugin_slurm
```

- Singularity container platform
- MMORF singularity container for the GPU-based nonlinear registrations - can be downloaded from https://git.fmrib.ox.ac.uk/flange/mmorf_beta.
- The latest version of FSL - more information on how to install it can be found on https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation.
- Jobs are submitted to the cluster with `fsl_sub` which supports SGE and Slurm job management.

## Running the template construction pipeline
Cloning this repository will provide you with the main template construction pipeline (`run_template_construction.py`), and a directory with config files (`tempconfig`).
These config files contain information about the data structure (`data_structure.tree`) and nonlinear registration parameters (`mmorf_params_step<0..6>.ini`) necessary for the pipeline to function correctly.

```bash
git clone https://git.fmrib.ox.ac.uk/cart/mm-template-construction.git
cd mm-template-construction

python run_template_construction.py [Options]
```

For the pipeline to know which modalities to use and where the images are located, the data structure has to be specified in the file-tree `data_structure.tree`
(more information about file trees can be found on https://open.win.ox.ac.uk/pages/ndcn0236/file-tree/). The three sections under data_dir, config_dir and preprocesing_dir can be edited to define
the input data structure. The section under template_dir defines the output structure and should not be changed. I will describe the structure and how it can be changed on the following example:

Let's assume we would like to construct a template from 10 subjects with 3 modalities T1, T2 FLAIR and dMRI, which is organised in the following way:

```bash
/studies/test_study/imaging_data/
        + subject_1
              + T1
                 |-- T1_unbiased_brain.nii.gz
                 |-- T1_brain_mask.nii.gz
                 |-- T1_unbiased_head.nii.gz
              + T2_FLAIR
                 |-- T2_FLAIR_unbiased_head.nii.gz
              + dMRI
                 |-- dti_tensor.nii.gz
       .
       .
       .
        + subject_10
              + T1
                 |-- T1_unbiased_brain.nii.gz
                 |-- T1_brain_mask.nii.gz
                 |-- T1_unbiased_head.nii.gz
              + T2_FLAIR
                 |-- T2_FLAIR_unbiased_head.nii.gz
              + dMRI
                 |-- dti_tensor.nii.gz
```

This directory structure can be described in our file-tree by:
```bash
!{data_dir}
    {sub_id} (data_sub_dir)
        T1 (sub_T1_dir)
            T1_unbiased_brain{ext_nii} (T1_brain)
            T1_brain_mask{ext_nii} (T1_brain_mask)
            T1_unbiased_head{ext_nii} (T1_head)
        T2_FLAIR (sub_T2_dir)
            T2_FLAIR_unbiased_head{ext_nii} (T2_head)
        dMRI (sub_dMRI_dir)
            dti_tensor{ext_nii} (DTI_tensor)
```
Note that the modalities used for template construction are defined in this file-tree i.e. removing, for example, dMRI will only create a T1+T2 template. While the directory structure can be edited by changing the indents, the keys such as 'T1_brain', 'T1_brain_mask' etc. can not be changed.

The parameters for the nonlinear registrations at each resolution level are described in the config_dir. This repository already includes a basic set of parameter files in the folder 'tempconfig' which can be pointed to:
```bash
!{config_dir}
    mmorf_params_step01{ext_ini} (mmorf_params_01)
    mmorf_params_step02{ext_ini} (mmorf_params_02)
    mmorf_params_step03{ext_ini} (mmorf_params_03)
    mmorf_params_step04{ext_ini} (mmorf_params_04)
    mmorf_params_step05{ext_ini} (mmorf_params_05)
    mmorf_params_step06{ext_ini} (mmorf_params_06)
```

Additionally the pipeline requires rigid transformations between the modalities of a subject e.g. between the T2 and T1 scans of one subject. Their location has to be specified with the keys 'T2_to_T1_mat' or 'DTI_to_T1_mat' in the data_dir section or in a separate
preprocessed_dir. 
```bash
!{preprocessed_dir}
    {sub_id}_T2_to_T1{ext_mat} (T2_to_T1_mat)
    {sub_id}_DTI_to_T1{ext_mat} (DTI_to_T1_mat)
```

`!{data_dir}`, `!{config_dir}` and `!{preprocessed_dir}` will be replaced with directory paths provided by the `--input`, `--config` and `--preprocessed` options respectively. `{sub_id}` will 
be replaced with the folder name of the subject.

### Options
####   `-h, --help`
Show help message

####   `-i <dir>, --input <dir>`
Absolute path to directory containing the subjects/timepoints

####   `-config <dir>, --config <dir>`
Absolute path to directory containing the mmorf_params_step<0..6>.ini files e.g. /path/to/tempconfig/

####   `-m <path>, --mmorf <path>`
Absolute path to mmorf.sif

####   `-t <path>, --tree <path>`
Path to FSL Filetree describing the subject-specific directory structure and selection of modalities. An example file-tree is located in the folder `tempconfig` and can be edited as described above.

Always required keys for T1: T1_brain, T1_head, T1_brain_mask
Optional keys when including T2: T2_head
Optional keys when including dMRI: dti_tensor

`{sub_id}` is a placeholder and will be automatically replaced with:
- IDs in the CSV file provided with `--subids <path>`
- if the `--subids <path>` flag is not provided, the sub-directories of `--input <dir>` will be used

####   `-p <dir>, --preprocessed <dir>`
Absolute path to directory containing rigid transformations

####   `-o <dir>, --output <dir>`
Absolute path to output directory

####   `-s <path>, --subids <path>`
Path to .csv file containing one subject ID per row: subject IDs have to indentify the sub-directories of the 
'input' argument (optional)

If not provided all sub-directories of the --input <dir> argument will be used as subject IDs

####   `-aff [True,False], --affine [True,False]`
Run affine template construction (required for affine template construction)

####   `-nln [True,False], --nonlinear [True,False]`
Run nonlinear template construction (required for nonlinear template construction)

####   `-c <string>, --cpuq <string>`
Name of a cluster queue to submit CPU jobs to (required for affine and nonlinear template construction)

####   `-g <string>, --gpuq <string>`
Name of a cluster queue to submit GPU jobs to (required for nonlinear template construction)

####   `-nres <int>, --n_resolutions <int>`
Number of resolution levels (required for nonlinear template construction)

####   `-nit <int>, --n_iterations <int>`
Number of iterations per resolution level (required for nonlinear template construction)

### Example
A typical command for multimodal template construction given multiple subjects would look like this:
```bash
python run_template_construction.py \
    -i /path/to/imaging/data/subjectsAll/ \
    --config /path/to/mm_template_construction/tempconfig \
    -t /path/to/mm_template_construction/tempconfig/data_structure.tree \
    -o /path/to/imaging/data/multimodal_template/ \
    -p /path/to/imaging/data/affine/ \
    --cpuq cpu.q --gpuq gpu.q \
    -aff True -nln True \
    -nres 6 -nit 3 \
    -s ./data/sub_ids.csv \
    -m /path/to/mmorf.sif
```

## Output
Output directories will be automatically created for different purposes. These include:
 
####    `log`
Contains log-files from the job submission system; this is very useful for debugging

###     `scripts`
Contains generated Bash and Python scripts e.g. for job array submissions, wrappers for Python functions

####    `misc`
Contains useful files like an identity transform

####    `clamped_images`
Contains intensity clamped T1 images to reduce hyperintensities sometimes noticeable e.g. in the skull

####    `affine_step1`
Contains the output of the first set of rigd and affine registrations including 
- affine registrations between each subject and a reference subject
- unbiasing transformation removing the bias introduced by the reference subject
- unbiased average T1 template in the unbiased space of the subjects
- rigid transformation of unbiased template to MNI space

####    `affine_step2`
Contains the transformed images in MNI space and the affine T1, T2, DTI volumes of the template

####    `nln_step_<0..6>`
Contains the warps, bias fields, Jacobians and transformed images as well as the template generated at the corresponding resolution level.



